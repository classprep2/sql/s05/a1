-- 1
SELECT customerName FROM customers 
WHERE country = "Philippines";

-- 2
SELECT contactLastName FROM customers 
WHERE customerName = "La Rochelle Gifts";

-- 3
SELECT productName, MSRP FROM products 
WHERE productName = "The Titanic";

-- 4
SELECT firstName, lastName FROM employees 
WHERE email = "jfirrelli@classicmodelcars.com";

-- 5
SELECT customerName FROM customers
WHERE state is NULL;

-- 6
SELECT firstName, lastName, email FROM employees 
WHERE lastName = "Patterson" 
	AND firstName = "Steve";

-- 7
SELECT customerName, country, creditLimit FROM customers
WHERE country != 'USA'
AND creditLimit > 3000;


-- 8 
SELECT customerNumber FROM orders
WHERE comments LIKE '%DHL%' ;

-- 9
SELECT productLine FROM products
WHERE productDescription LIKE '%state of the art%' ;

-- 10
SELECT DISTINCT country FROM customers;

-- 11
SELECT DISTINCT status FROM orders;

-- 12
 SELECT customerName, country FROM customers
 WHERE country IN ('USA', 'France', 'Canada');

-- 13
SELECT firstName, lastName, officeCode  FROM employees
WHERE officeCode = 5;

-- 14
SELECT customerName FROM customers
WHERE salesRepEmployeeNumber = 1166;

-- 15
SELECT productName, customerName 
FROM products 
JOIN orderDetails ON products.productCode = orderDetails.productCode 
JOIN orders ON orderDetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customerName = 'Baane Mini Imports';


-- 16
SELECT firstName, lastName, customerName, customers.country 
FROM employees 
JOIN offices ON employees.officeCode = offices.officeCode 
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
WHERE customers.country = offices.country;

-- 17
SELECT productName, quantityInStock 
FROM products 
WHERE productLine = 'Planes' 
	AND quantityInStock < 1000;

-- 18
SELECT customerName, phone 
FROM customers 
WHERE phone LIKE "+81%";